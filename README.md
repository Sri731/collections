# Collections App
## _Optimized for Mobile & Tablet (Both Potrait and Landscape orientations)_

As a successful Shopify merchant with many collections of products, this app displays a Custom Collections list page and a Collection Details page
to help you keep an eye on your collections.
Data is fetched from the Shopify Custom Collections REST API.


**Custom Collections list page**:
A simple list of every custom collection.
Tapping on a collection launches the Collection Details page.

**Collection Details page**:
A list of every product for a specific collection.
Includes collection details in a card at the top that contains the image, title and body_html of the selected collection.
Each row in the list contains:

        *  The name of the product
        *  The total available inventory across all variants of the product
        *  The collection title
        *  The collection image

# Dependencies
        * Android Gradle Plugin
        * Android Support Library (v7)
        * RecyclerView
        * CardView
        * LiveData
        * ViewModel
        * OkHttp (for logging purposes)
        * Retrofit2
        * Retrofit2-gson
        * Glide

# Setup
The Shopify Custom Collections REST API requires an access token for
any data request. Include your access token in the `gradle.properties`
file, which is found at the root project level. If not, create one with
the same name. In the said file, simply create a variable
with the name access token and pass in your token.
```
ACESS_TOKEN = "your token here"
```
The app requires a valid access token and internet connection to display
data.


# Screenshots
![screenshot](https://bitbucket.org/Sri731/collections/raw/master/screenshots/mobile_potrait_1.png)
![screenshot](https://bitbucket.org/Sri731/collections/raw/master/screenshots/mobile_detail_potrait_1.png)
![screenshot](https://bitbucket.org/Sri731/collections/raw/master/screenshots/mobile_landscape.png)
![screenshot](https://bitbucket.org/Sri731/collections/raw/master/screenshots/mobile_detail_landscape.png)
![screenshot](https://bitbucket.org/Sri731/collections/raw/master/screenshots/tablet_potrait_1.png)
![screenshot](https://bitbucket.org/Sri731/collections/raw/master/screenshots/tablet_potrait_2.png)
![screenshot](https://bitbucket.org/Sri731/collections/raw/master/screenshots/tablet_landscape_1.png)
![screenshot](https://bitbucket.org/Sri731/collections/raw/master/screenshots/tablet_landscape_2.png)

