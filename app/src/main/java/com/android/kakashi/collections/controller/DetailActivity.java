package com.android.kakashi.collections.controller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.android.kakashi.collections.R;
import com.android.kakashi.collections.model.CustomCollection;
import com.android.kakashi.collections.view.CollectionDetailFragment;

public class DetailActivity extends AppCompatActivity {

	private static final String EXTRA_COLLECTION_OBJECT =
			"com.android.kakashi.collections.collection";

	public static Intent newIntent(Context packageContext, CustomCollection collection) {
		Intent intent = new Intent(packageContext, DetailActivity.class);
		intent.putExtra(EXTRA_COLLECTION_OBJECT, collection);
		return intent;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail);

		CustomCollection collection = getIntent().getParcelableExtra(EXTRA_COLLECTION_OBJECT);

		final FragmentManager fm = getSupportFragmentManager();
		Fragment fragment = fm.findFragmentById(R.id.detail_fragment_container);

		if (fragment == null) {
			fragment = CollectionDetailFragment.newInstance(collection);
			fm.beginTransaction()
					.add(R.id.detail_fragment_container, fragment)
					.commit();
		}
	}
}
