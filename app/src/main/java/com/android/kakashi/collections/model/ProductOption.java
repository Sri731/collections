package com.android.kakashi.collections.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class ProductOption {
	@SerializedName("id")
	@Expose
	private String id;
	@SerializedName("product_id")
	@Expose
	private String productId;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("position")
	@Expose
	private Integer position;
	@SerializedName("values")
	@Expose
	private List<String> values = null;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public List<String> getValues() {
		return values;
	}

	public void setValues(List<String> values) {
		this.values = values;
	}
}
