package com.android.kakashi.collections.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class CustomCollectionResponse {
	@SerializedName("custom_collections")
	@Expose
	private List<CustomCollection> customCollections = null;

	public List<CustomCollection> getCustomCollections() {
		return customCollections;
	}

	public void setCustomCollections(List<CustomCollection> customCollections) {
		this.customCollections = customCollections;
	}
}
