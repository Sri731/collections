package com.android.kakashi.collections.api;

import com.android.kakashi.collections.BuildConfig;
import com.android.kakashi.collections.model.CollectionProductResponse;
import com.android.kakashi.collections.model.CustomCollectionResponse;
import com.android.kakashi.collections.model.Products;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ShopifyService {

	String ACCESS_TOKEN = BuildConfig.ACCESS_TOKEN;

	@GET("/admin/custom_collections.json?access_token=" + ACCESS_TOKEN)
	Call<CustomCollectionResponse> makeAPIRequest();

	@GET("/admin/collects.json?access_token=" + ACCESS_TOKEN)
	Call<CollectionProductResponse> getCollectionProductMapping(
			@Query("collection_id") String id
	);

	@GET("/admin/products.json?access_token=" + ACCESS_TOKEN)
	Call<Products> getProductsForCollection(
			@Query("ids") String productIDsForCollection
	);
}
