package com.android.kakashi.collections;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.android.kakashi.collections.model.CustomCollection;

public class DetailViewModelFactory extends ViewModelProvider.NewInstanceFactory {

	private CustomCollection collection;

	public DetailViewModelFactory(CustomCollection collection) {
		this.collection = collection;
	}

	@NonNull
	@Override
	@SuppressWarnings("unchecked")
	public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
		return (T) new DetailViewModel(collection);
	}
}
