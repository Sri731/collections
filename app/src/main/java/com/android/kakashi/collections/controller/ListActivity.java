package com.android.kakashi.collections.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.android.kakashi.collections.R;
import com.android.kakashi.collections.model.CustomCollection;
import com.android.kakashi.collections.view.CollectionDetailFragment;
import com.android.kakashi.collections.view.CollectionsListFragment;

public class ListActivity
		extends AppCompatActivity
		implements CollectionsListFragment.Callbacks {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_masterdetail);

		final FragmentManager fm = getSupportFragmentManager();

		Fragment fragment = fm.findFragmentById(R.id.fragment_container);

		if (fragment == null) {
			fragment = CollectionsListFragment.newInstance();
			fm.beginTransaction()
					.add(R.id.fragment_container, fragment)
					.commit();
		}
	}

	@Override
	public void onCollectionSelected(CustomCollection collection) {
		if (findViewById(R.id.detail_fragment_container) == null) {
			Intent intent = DetailActivity.newIntent(this, collection);
			startActivity(intent);
		} else {
			Fragment newDetail = CollectionDetailFragment.newInstance(collection);

			getSupportFragmentManager().beginTransaction()
					.replace(R.id.detail_fragment_container, newDetail)
					.commit();
		}
	}
}
