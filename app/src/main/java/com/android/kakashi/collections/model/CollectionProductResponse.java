package com.android.kakashi.collections.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class CollectionProductResponse {

	@SerializedName("collects")
	@Expose
	private List<CollectionProductMap> collects = null;

	public List<CollectionProductMap> getCollects() {
		return collects;
	}

	public void setCollects(List<CollectionProductMap> collects) {
		this.collects = collects;
	}
}
