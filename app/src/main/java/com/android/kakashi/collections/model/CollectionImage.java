package com.android.kakashi.collections.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CollectionImage implements Parcelable {
	@SerializedName("created_at")
	@Expose
	private String createdAt;
	@SerializedName("alt")
	@Expose
	private Object alt;
	@SerializedName("width")
	@Expose
	private Integer width;
	@SerializedName("height")
	@Expose
	private Integer height;
	@SerializedName("src")
	@Expose
	private String src;

	protected CollectionImage(Parcel in) {
		createdAt = in.readString();
		if (in.readByte() == 0) {
			width = null;
		} else {
			width = in.readInt();
		}
		if (in.readByte() == 0) {
			height = null;
		} else {
			height = in.readInt();
		}
		src = in.readString();
	}

	public static final Creator<CollectionImage> CREATOR = new Creator<CollectionImage>() {
		@Override
		public CollectionImage createFromParcel(Parcel in) {
			return new CollectionImage(in);
		}

		@Override
		public CollectionImage[] newArray(int size) {
			return new CollectionImage[size];
		}
	};

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public Object getAlt() {
		return alt;
	}

	public void setAlt(Object alt) {
		this.alt = alt;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public String getSrc() {
		return src;
	}

	public void setSrc(String src) {
		this.src = src;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(createdAt);
		if (width == null) {
			dest.writeByte((byte) 0);
		} else {
			dest.writeByte((byte) 1);
			dest.writeInt(width);
		}
		if (height == null) {
			dest.writeByte((byte) 0);
		} else {
			dest.writeByte((byte) 1);
			dest.writeInt(height);
		}
		dest.writeString(src);
	}
}
