package com.android.kakashi.collections.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.kakashi.collections.GlideApp;
import com.android.kakashi.collections.R;
import com.android.kakashi.collections.model.Product;
import com.android.kakashi.collections.model.ProductVariant;

import java.util.List;

import static com.android.kakashi.collections.adapter.ProductAdapter.ProductViewHolder;

public class ProductAdapter extends RecyclerView.Adapter<ProductViewHolder> {

	private List<Product> products;
	private Context context;

	public ProductAdapter(Context context, @Nullable List<Product> products) {
		this.context = context;
		this.products = products;
	}

	@NonNull
	@Override
	public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
		View itemView = LayoutInflater.from(parent.getContext()).inflate(
				R.layout.collection_detail_list_item,
				parent,
				false
		);
		return new ProductViewHolder(itemView);
	}

	@Override
	public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
		Product currentProduct = products.get(position);

		// determine total inventory across variants
		int inventory = 0;
		List<ProductVariant> variants = currentProduct.getVariants();
		for (ProductVariant productVariant :
				variants) {
			inventory += productVariant.getInventoryQuantity();
		}

		// product image
		GlideApp.with(context)
				.load(currentProduct.getImage().getSrc())
				.into(holder.productImageView);

		holder.bindData(
				currentProduct.getTitle(),
				currentProduct.getBodyHtml(),
				currentProduct.getProductType(),
				String.valueOf(inventory)
		);
	}

	@Override
	public int getItemCount() {
		return products == null ? 0 : products.size();
	}

	public void addData(List<Product> products) {
		this.products = products;
		notifyDataSetChanged();
	}

	static class ProductViewHolder extends RecyclerView.ViewHolder {

		TextView productTitleTextView;
		TextView productDescTextView;
		TextView productTypeTextView;
		TextView productInventoryTextView;
		ImageView productImageView;

		public ProductViewHolder(@NonNull View itemView) {
			super(itemView);

			productTitleTextView = itemView.findViewById(R.id.product_title);
			productDescTextView = itemView.findViewById(R.id.product_desc);
			productTypeTextView = itemView.findViewById(R.id.product_type);
			productInventoryTextView = itemView.findViewById(R.id.product_inventory);
			productImageView = itemView.findViewById(R.id.product_image_view);
		}

		void bindData(String productTitle, String desc, String type, String inventory) {
			String inventoryDisplay = "Inventory: " + inventory;

			productTitleTextView.setText(productTitle);
			productDescTextView.setText(desc);
			productTypeTextView.setText(type);
			productInventoryTextView.setText(inventoryDisplay);
		}
	}
}
