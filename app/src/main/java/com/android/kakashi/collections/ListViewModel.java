package com.android.kakashi.collections;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.kakashi.collections.api.Client;
import com.android.kakashi.collections.api.ShopifyService;
import com.android.kakashi.collections.model.CustomCollection;
import com.android.kakashi.collections.model.CustomCollectionResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListViewModel extends ViewModel {

	private static final String LOG_TAG = ListViewModel.class.getSimpleName();

	private MutableLiveData<List<CustomCollection>> collections;

	public LiveData<List<CustomCollection>> getCollections() {
		if (collections == null) {
			collections = new MutableLiveData<>();
			getCollectionsFromAPI();
		}

		return collections;
	}

	private void getCollectionsFromAPI() {
		ShopifyService apiRequest = Client.getClient().create(ShopifyService.class);
		Call<CustomCollectionResponse> call = apiRequest.makeAPIRequest();

		// async request fulfillment
		call.enqueue(new Callback<CustomCollectionResponse>() {
			@Override
			public void onResponse(@NonNull Call<CustomCollectionResponse> call,
			                       @NonNull Response<CustomCollectionResponse> response) {

				Log.d(LOG_TAG, "OnResponse() -> Status code: " + response.code());
				if (response.isSuccessful() && response.body() != null) {
					collections.setValue(response.body().getCustomCollections());
				}
			}

			@Override
			public void onFailure(@NonNull Call<CustomCollectionResponse> call,
			                      @NonNull Throwable t) {
				Log.d(LOG_TAG, "onFailure() -> Status code: " + t.toString());
				t.getMessage();
				t.getStackTrace();
			}
		});
	}
}
