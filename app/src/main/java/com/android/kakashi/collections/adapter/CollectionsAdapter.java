package com.android.kakashi.collections.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.kakashi.collections.R;
import com.android.kakashi.collections.adapter.CollectionsAdapter.CollectionViewHolder;
import com.android.kakashi.collections.model.CustomCollection;

import java.util.List;

public class CollectionsAdapter extends RecyclerView.Adapter<CollectionViewHolder> {

	private List<CustomCollection> data;
	private CollectionItem collectionClickListener;

	public interface CollectionItem {
		void clickListener(CustomCollection collection);
	}

	public CollectionsAdapter(CollectionItem collectionClickListener) {
		this.collectionClickListener = collectionClickListener;
	}

	@NonNull
	@Override
	public CollectionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
		View rootView = LayoutInflater.from(parent.getContext())
				                    .inflate(R.layout.collections_list_item, parent, false);
		return new CollectionViewHolder(rootView, collectionClickListener, data);
	}

	@Override
	public void onBindViewHolder(@NonNull CollectionViewHolder holder, int position) {
		holder.bindDataToView(data.get(position));
	}

	@Override
	public int getItemCount() {
		return data == null ? 0 : data.size();
	}

	public void addData(List<CustomCollection> data) {
		this.data = data;
		notifyDataSetChanged();
	}

	static class CollectionViewHolder
			extends RecyclerView.ViewHolder
			implements View.OnClickListener {

		private TextView titleTextView;
		private List<CustomCollection> data;
		private CollectionItem collectionClickListener;

		CollectionViewHolder(@NonNull View itemView,
		                     CollectionItem collectionClickListener, List<CustomCollection> data) {
			super(itemView);
			itemView.setOnClickListener(this);
			this.collectionClickListener = collectionClickListener;
			this.data = data;

			titleTextView = itemView.findViewById(R.id.col_title);
		}

		void bindDataToView(CustomCollection collection) {
			titleTextView.setText(collection.getTitle());
		}

		@Override
		public void onClick(View v) {
			CustomCollection collection = data.get(getAdapterPosition());
			collectionClickListener.clickListener(collection);
		}
	}
}
