package com.android.kakashi.collections.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CustomCollection implements Parcelable {
	@SerializedName("id")
	@Expose
	private String id;
	@SerializedName("handle")
	@Expose
	private String handle;
	@SerializedName("title")
	@Expose
	private String title;
	@SerializedName("updated_at")
	@Expose
	private String updatedAt;
	@SerializedName("body_html")
	@Expose
	private String bodyHtml;
	@SerializedName("published_at")
	@Expose
	private String publishedAt;
	@SerializedName("sort_order")
	@Expose
	private String sortOrder;
	@SerializedName("template_suffix")
	@Expose
	private String templateSuffix;
	@SerializedName("published_scope")
	@Expose
	private String publishedScope;
	@SerializedName("admin_graphql_api_id")
	@Expose
	private String adminGraphqlApiId;
	@SerializedName("image")
	@Expose
	private CollectionImage image;

	protected CustomCollection(Parcel in) {
		id = in.readString();
		handle = in.readString();
		title = in.readString();
		updatedAt = in.readString();
		bodyHtml = in.readString();
		publishedAt = in.readString();
		sortOrder = in.readString();
		templateSuffix = in.readString();
		publishedScope = in.readString();
		adminGraphqlApiId = in.readString();
		image = in.readParcelable(CollectionImage.class.getClassLoader());
	}

	public static final Creator<CustomCollection> CREATOR = new Creator<CustomCollection>() {
		@Override
		public CustomCollection createFromParcel(Parcel in) {
			return new CustomCollection(in);
		}

		@Override
		public CustomCollection[] newArray(int size) {
			return new CustomCollection[size];
		}
	};

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getHandle() {
		return handle;
	}

	public void setHandle(String handle) {
		this.handle = handle;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getBodyHtml() {
		return bodyHtml;
	}

	public void setBodyHtml(String bodyHtml) {
		this.bodyHtml = bodyHtml;
	}

	public String getPublishedAt() {
		return publishedAt;
	}

	public void setPublishedAt(String publishedAt) {
		this.publishedAt = publishedAt;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getTemplateSuffix() {
		return templateSuffix;
	}

	public void setTemplateSuffix(String templateSuffix) {
		this.templateSuffix = templateSuffix;
	}

	public String getPublishedScope() {
		return publishedScope;
	}

	public void setPublishedScope(String publishedScope) {
		this.publishedScope = publishedScope;
	}

	public String getAdminGraphqlApiId() {
		return adminGraphqlApiId;
	}

	public void setAdminGraphqlApiId(String adminGraphqlApiId) {
		this.adminGraphqlApiId = adminGraphqlApiId;
	}

	public CollectionImage getImage() {
		return image;
	}

	public void setImage(CollectionImage image) {
		this.image = image;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(id);
		dest.writeString(handle);
		dest.writeString(title);
		dest.writeString(updatedAt);
		dest.writeString(bodyHtml);
		dest.writeString(publishedAt);
		dest.writeString(sortOrder);
		dest.writeString(templateSuffix);
		dest.writeString(publishedScope);
		dest.writeString(adminGraphqlApiId);
		dest.writeParcelable(image, flags);
	}
}
