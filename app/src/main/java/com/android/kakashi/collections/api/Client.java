package com.android.kakashi.collections.api;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class Client {

	private static Retrofit retrofit = null;
	private static final String BASE_URL = "https://shopicruit.myshopify.com";

	private Client() {
		// Not an instantiable class
	}

	public static Retrofit getClient() {
		HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
		interceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);

		OkHttpClient client = new OkHttpClient.Builder()
				                      .addInterceptor(interceptor)
				                      .build();

		if (retrofit == null) {
			retrofit = new Retrofit.Builder()
					.baseUrl(BASE_URL)
					.addConverterFactory(GsonConverterFactory.create())
		            .client(client)
					.build();
		}

		return retrofit;
	}
}
