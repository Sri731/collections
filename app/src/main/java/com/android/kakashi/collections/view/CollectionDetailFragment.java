package com.android.kakashi.collections.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.kakashi.collections.DetailViewModel;
import com.android.kakashi.collections.DetailViewModelFactory;
import com.android.kakashi.collections.GlideApp;
import com.android.kakashi.collections.R;
import com.android.kakashi.collections.adapter.ProductAdapter;
import com.android.kakashi.collections.model.CustomCollection;
import com.android.kakashi.collections.model.Product;

import java.util.List;

public class CollectionDetailFragment extends Fragment {

	private static final String ARG_COLLECTION = "collection";
	private static final String LOG_TAG = CollectionDetailFragment.class.getSimpleName();

	private CustomCollection collection;
	private ProductAdapter adapter;

	public static CollectionDetailFragment newInstance(CustomCollection collection) {
		Bundle args = new Bundle();
		args.putParcelable(ARG_COLLECTION, collection);

		CollectionDetailFragment fragment = new CollectionDetailFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
			collection = getArguments().getParcelable(ARG_COLLECTION);
		} else {
			Log.d(LOG_TAG, "List of Products Missing...");
		}

		// attach view mode
		DetailViewModel viewModel = ViewModelProviders.of(
				this,
				new DetailViewModelFactory(collection)
		).get(DetailViewModel.class);

		viewModel.getProducts().observe(this, new Observer<List<Product>>() {
			@Override
			public void onChanged(@Nullable List<Product> products) {
				adapter.addData(products);
			}
		});
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup parent,
	                         @Nullable Bundle savedInstanceState) {
		// RecyclerView displaying list of products belonging to the selected collection
		View fragLayout = inflater.inflate(R.layout.fragment_collection_detail, parent, false);

		// collection title
		TextView collectionTitleTextView = fragLayout.findViewById(R.id.collection_title);
		collectionTitleTextView.setText(collection.getTitle());

		// display collection description
		TextView collectionDescriptionTextView = fragLayout.findViewById(R.id.collection_desc);
		collectionDescriptionTextView.setText(collection.getBodyHtml());

		// collection image parse and load
		ImageView collectionImageView = fragLayout.findViewById(R.id.collection_image);
		GlideApp.with(requireActivity())
				.load(collection.getImage().getSrc())
				.into(collectionImageView);

		// Products recycler view
		RecyclerView recyclerView = fragLayout.findViewById(R.id.collection_products_recycler_view);
		recyclerView.setHasFixedSize(true);
		recyclerView.setLayoutManager(new LinearLayoutManager(
				getActivity(),
				LinearLayout.VERTICAL,
				false
		));

		adapter = new ProductAdapter(getActivity(), null);
		recyclerView.setAdapter(adapter);

		return fragLayout;
	}
}
