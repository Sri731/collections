package com.android.kakashi.collections;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.kakashi.collections.api.Client;
import com.android.kakashi.collections.api.ShopifyService;
import com.android.kakashi.collections.model.CollectionProductMap;
import com.android.kakashi.collections.model.CollectionProductResponse;
import com.android.kakashi.collections.model.CustomCollection;
import com.android.kakashi.collections.model.Product;
import com.android.kakashi.collections.model.Products;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailViewModel extends ViewModel {

	private static final String LOG_TAG = DetailViewModel.class.getSimpleName();

	private CustomCollection collection;
	private MutableLiveData<List<Product>> products;

	public DetailViewModel(CustomCollection collection) {
		this.collection = collection;
	}

	public LiveData<List<Product>> getProducts() {
		if (products == null) {
			products = new MutableLiveData<>();
			getProductsForCollection();
		}

		return products;
	}

	private void getProductsFromAPI(String ids) {
		ShopifyService apiClient = Client.getClient().create(ShopifyService.class);
		Call<Products> productsCall = apiClient.getProductsForCollection(ids);

		productsCall.enqueue(new Callback<Products>() {
			@Override
			public void onResponse(@NonNull Call<Products> call,
			                       @NonNull Response<Products> response) {
				if (response.isSuccessful() && response.body() != null) {
					Products productsResponse = response.body();
					products.setValue(productsResponse.getProductList());
				}
			}

			@Override
			public void onFailure(@NonNull Call<Products> call,
			                      @NonNull Throwable t) {
				Log.d(LOG_TAG, "ProductsCall: onFailure() -> Status code: " + t.toString());
				t.getMessage();
				t.getStackTrace();
			}
		});
	}

	private void getProductsForCollection() {
		final StringBuilder sb = new StringBuilder();

		final ShopifyService apiClient = Client.getClient().create(ShopifyService.class);
		Call<CollectionProductResponse> productCollectionCall = apiClient.getCollectionProductMapping(collection.getId());

		productCollectionCall.enqueue(new Callback<CollectionProductResponse>() {
			@Override
			public void onResponse(@NonNull Call<CollectionProductResponse> call,
			                       @NonNull Response<CollectionProductResponse> response) {

				if (response.isSuccessful() && response.body() != null) {
					List<CollectionProductMap> mappings = response.body().getCollects();


					for (int i = 0; i < mappings.size(); i++) {
						sb.append(mappings.get(i).getProductId());
						if (i < mappings.size() - 1) {
							sb.append(",");
						}
					}

					getProductsFromAPI(sb.toString());
				}
			}

			@Override
			public void onFailure(@NonNull Call<CollectionProductResponse> call,
			                      @NonNull Throwable t) {
				Log.d(LOG_TAG, "productCollectionCall: onFailure() -> Status code: " + t.toString());
				t.getMessage();
				t.getStackTrace();
			}
		});
	}
}
