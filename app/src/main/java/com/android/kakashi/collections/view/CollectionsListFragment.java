package com.android.kakashi.collections.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.kakashi.collections.ListViewModel;
import com.android.kakashi.collections.R;
import com.android.kakashi.collections.adapter.CollectionsAdapter;
import com.android.kakashi.collections.model.CustomCollection;

import java.util.List;

public class CollectionsListFragment
		extends Fragment
		implements CollectionsAdapter.CollectionItem {

	@SuppressWarnings("FieldCanBeLocal")
	private RecyclerView recyclerView;
	private CollectionsAdapter adapter;
	private Callbacks callbacks;

	public interface Callbacks {
		void onCollectionSelected(CustomCollection collection);
	}

	public static CollectionsListFragment newInstance() {
		return new CollectionsListFragment();
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		callbacks = (Callbacks) context;
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
	                         @Nullable Bundle savedInstanceState) {
		View fragView = inflater.inflate(R.layout.fragment_collections_list, container, false);
		readyAdapterView(fragView);

		// attach view model
		ListViewModel viewModel = ViewModelProviders.of(this)
				                                  .get(ListViewModel.class);
		viewModel.getCollections().observe(this, new Observer<List<CustomCollection>>() {
			@Override
			public void onChanged(@Nullable List<CustomCollection> collections) {
				adapter.addData(collections);
			}
		});

		return fragView;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		callbacks = null;
	}

	private void readyAdapterView(View rootView) {
		recyclerView = rootView.findViewById(R.id.recycler_view);
		recyclerView.setHasFixedSize(true);
		recyclerView.setLayoutManager(new GridLayoutManager(
				getActivity(),
				2
		));

		adapter = new CollectionsAdapter(this);
		recyclerView.setAdapter(adapter);

	}

	@Override
	public void clickListener(CustomCollection collection) {
		callbacks.onCollectionSelected(collection);
	}
}
